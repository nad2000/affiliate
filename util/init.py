import django
import os
from django.core.wsgi import get_wsgi_application

os.environ["DJANGO_SETTINGS_MODULE"] = "config.settings"
application = get_wsgi_application()

def CreateTenant(schemaName, name, domain):
  from apps.customers.models import Client, Domain

  '''
  Create your public tenant
  Should not use 
    tenant = Client.objects.get_or_create(schema_name=schemaName, name=name)[0]
  django-tenants create schemas in Client.save() when Client.pk is None (this client does not exist yet).
  Client.pk is non None when call Client.objects.get_or_create() and schema cannot be created.
  '''
  if Client.objects.filter(schema_name=schemaName).count()==0:
    tenant = Client()
    tenant.schema_name = schemaName
    tenant.name = name
    tenant.save()
  tenant = Client.objects.get(schema_name=schemaName)

  # Add one or more domains for the tenant
  # don't add your port or www here! on a local server you'll want to use localhost here
  domain = Domain.objects.get_or_create(domain=domain, tenant=tenant, is_primary=True)[0]
  # domain = Domain()
  # domain.domain = domain
  # domain.tenant = tenant
  # domain.is_primary = True
  domain.save()

CreateTenant(schemaName='public', name='Schemas Inc.', domain='topcarcamera.com')
t = 'tenant1'; CreateTenant(schemaName=t, name=t, domain='%s.topcarcamera.com'%t)
t = 'tenant2'; CreateTenant(schemaName=t, name=t, domain='%s.topcarcamera.com'%t)
