// [react to onchange event](https://djangosteps.wordpress.com/2012/01/12/filtered-menus-in-django/)
// set up a new XMLHttpRequest variable
var request = false;
try {
  request = new XMLHttpRequest();
} catch (trymicrosoft) {
  try {
    request = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (othermicrosoft) {
    try {
      request = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (failed) {
      request = false;
    }
  }
}
 
if (!request)
  alert("Error initializing XMLHttpRequest!");
 
function GetProductPrice() {
  // seems that each element is automatically assigned an ID [](http://www.cnblogs.com/BeginMan/p/3306081.html)
  var providerId= document.getElementById("id_provider").value;
  var url = "http://" + window.location.host + "/ecmanager/GetProductPrice/" + providerId;
  request.open("GET", url, true);
  request.onreadystatechange = UpdatePage;
  request.send(null);
} 
 
// what to do when http ready state changes
function UpdatePage() { 
  if (request.readyState == 4) {
    if (request.status == 200) {
      var productPrice = request.responseText; 
      document.getElementById('id_productPriceForCurrentOrder').value = productPrice;
    }  
    else if (request.status == 404) {
      alert("Request url does not exist");
    }
    else {
      alert("Error: status code is " + request.status);
    }
  }
}   

