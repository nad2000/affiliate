#!/bin/bash
  # need to run this script using 'source' (source ./install.sh) [](http://stackoverflow.com/questions/7369145/activating-a-virtualenv-using-a-shell-script-doesnt-seem-to-work)

# base
mkdir ../logs
sudo apt-get install libmysqlclient-dev
sudo apt-get install mysql-server
sudo apt-get install python-pip python-dev build-essential
sudo pip install virtualenvwrapper 
sudo apt-get install gcc
sudo pip install --upgrade setuptools
sudo apt-get install libjpeg-dev
sudo apt-get install python-dateutil python-docutils python-feedparser python-gdata python-jinja2 python-ldap python-libxslt1 python-lxml python-mako python-mock python-openid python-psycopg2 python-psutil python-pybabel python-pychart python-pydot python-pyparsing python-reportlab python-simplejson python-tz python-unittest2 python-vatnumber python-vobject python-webdav python-werkzeug python-xlwt python-yaml python-zsi
sudo apt-get -y install libz-dev libfreetype6-dev python-dev

virtualenv venv
source venv/bin/activate
pip install ipython==4.0.0
pip install importlib==1.0.3
  # I have largely removed django-affiliate. No need to install django-affiliate to install some libs. 
  # pip install django-affiliate 
pip install Django==1.8.6
pip install python-keyczar==0.715 # for creating affiliate id

pip install django-registration==1.0
pip install django-crispy-forms==1.5.2
pip install peewee==2.6.4
pip install MySQL-python==1.2.5
pip install django-ipware==1.1.2
pip install coloredlogs==3.1.4

# multiple tenant
pip install django-tenants

# [parse gmail ](https://github.com/kennknowles/python-jsonpath-rw)
pip install jsonpath-rw==1.4.0
pip install objectpath==0.5
pip install python-dateutil==2.4.2
pip install pytz==2015.7
pip install django-simple-captcha==0.4.5
## for login gmail, get buka order data
pip install --upgrade google-api-python-client 

pip install dj_static
pip install dj-database-url

# for deployment
  #pip uninstall uwsgi
  #sudo apt-get remove uwsgi
pip install uwsgi

python manage.py migrate
