# -*- coding: utf-8 -*-
from django.contrib.auth.models import User as _User, AbstractUser
from django.db import models

class User(AbstractUser):
  USERNAME_FIELD = 'username'
  REQUIRED_FIELDS = ['email', 'award']

  award = models.IntegerField(default=0)

  def __unicode__(self):
    return self.username

  def GetRewarded(self):
    self.award += 10
    self.save()
