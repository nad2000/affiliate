# -*- coding: utf-8 -*-
from django.views.generic import CreateView
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.conf import settings
from django.http import HttpResponse

from .forms import UserForm
from .models import User


MIN_REQUEST_AMOUNT = getattr(settings, 'AFFILIATE_MIN_BALANCE_FOR_REQUEST', 0)

class UserCreateView(CreateView):
  model = User
  form_class = UserForm
  template_name = "account/signup.html"

  def get_success_url(self):
    # TODO: reverse
    return self.request.POST.get('next', reverse('products:ListProducts'))

  def form_valid(self, form):
    """
    If the form is valid, redirect to the supplied URL.
    """
    self.object = form.save()
    user = authenticate(
      username=form.cleaned_data['username'],
      password=form.cleaned_data['password'])
    login(self.request, user)
    return HttpResponseRedirect(self.get_success_url())

# [](https://docs.djangoproject.com/en/1.8/topics/class-based-views/generic-editing/)
from django.contrib.auth.decorators import login_required
@login_required
def EnableCurrentUserProductManagement(request):
  #SetPermission(request)
  # [](http://stackoverflow.com/questions/30683528/failed-to-to-turn-django-staff-flag-on-programatically)
  request.user.is_staff = True
  request.user.save()
  print ("EnableCurrentUserProductManagement")
  print ("request.user: %s" % request.user)
  print ("request.user.is_staff: %s" % request.user.is_staff)

  return WaitAndRedirect(request)
  #return HttpResponse('assign permission succeeded')

def SetPermission(request):
  from django.contrib.auth.models import Group, Permission
  from django.contrib.contenttypes.models import ContentType
  from config import settings
  try:
    content_type = ContentType.objects.get(app_label='users', model='User')
  except ObjectDoesNotExist:
    return HttpResponse('something is wrong. cannot create content_type')

  adminPermission = Permission.objects.get_or_create(
      codename='can_create_product', 
      name='Can create product', 
      content_type=content_type)[0]
  #user = User.objects.get(username='leonexu')
  request.user.user_permissions.add(adminPermission)

def WaitAndRedirect(request):
  from django.template import Template
  template = Template( 
      '''
      {% block extrahead %}
          assign permission succeeded
          <script type="text/javascript">
              setTimeout(function() {
                  window.location.href = "/users/List";
              }, 2000);
          </script>
      {% endblock %}
      '''
      )

  from django.template import RequestContext
  context = RequestContext(request, {})
  return HttpResponse(template.render(context))

from django.views.generic import ListView
class List(ListView):
  from apps.users.models import User
  template_name = "users/list.html"
  model = User
  #queryset = MyAffiliate.objects.select_related('user')
  context_object_name = 'users'
