#-*- coding: utf-8 -*-
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import ListView
import models

###### Provider
class CreateProvider(CreateView):
  model = models.Provider
  template_name = 'create.html'
  fields = ['url', 'product', 'productPrice', 'moq', 'canShipToChina', 'moqForCustomDesign', 'samplePrice']
  success_url = '/ecmanager/ListProviders'

class ListProviders(ListView):
  model = models.Provider
  template_name = "ecmanager/list_providers.html"
  context_object_name = 'providers'

class UpdateProvider(UpdateView):
  model = models.Provider
  template_name = 'update.html'
  success_url = '/ecmanager/ListProviders'
  fields = CreateProvider.fields + ['serviceQualityScore', 'serviceQualityReview']

###### Order
from models import Provider
class CreateOrder(CreateView):
  model = models.Order
  fields = ['provider', 'orderPrice', 'shippingCostInChina', 'shippingCostToIndonesia', 'trackingNumber', 'orderingDate', 'receivingDate', 'orderMaker', 'receiver', 'orderQuantity', 'status']

  template_name = 'create.html'
  success_url = '/ecmanager/ListOrders'

  # [Default values](http://stackoverflow.com/questions/16927144/createview-validate-form-and-default-fields)
  # does not work. 
  # def get_initial(self):
    # print ("dir(self.request.REQUEST): %s" % dir(self.request.REQUEST))
    # return { 'orderPrice': 1}

class ListOrders(ListView):
  model = models.Order
  template_name = "ecmanager/list_orders.html"
  context_object_name = 'orders'

class UpdateOrder(UpdateView):
  model = models.Order
  template_name = 'update.html'
  success_url = '/ecmanager/ListOrders'
  fields = CreateOrder.fields

class DeleteOrder(DeleteView):
  model = models.Order
  template_name = 'confirm_delete.html'
  success_url = '/ecmanager/ListOrders'

###### Ad
class CreateAd(CreateView):
  model = models.Ad
  fields = ['url', 'product', 'creator', 'comment']

  template_name = 'create.html'
  success_url = '/ecmanager/ListAds'

class ListAds(ListView):
  model = models.Ad
  template_name = "ecmanager/list_ads.html"
  context_object_name = 'ads'

class UpdateAd(UpdateView):
  model = models.Ad
  template_name = 'update.html'
  fields = CreateAd.fields
  success_url = '/ecmanager/ListAds'
