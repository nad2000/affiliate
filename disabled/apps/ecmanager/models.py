from django.db import models
from django.conf import settings
from apps.products.models import Product
from django.core.validators import MaxValueValidator, MinValueValidator
from decimal import Decimal
from django_tenants.models import TenantMixin, DomainMixin

class Provider(TenantMixin):
  url = models.URLField('Provider url', max_length=255)
  product = models.ForeignKey(Product, null=True)
  productPrice = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=False)
  moq = models.PositiveIntegerField(null=False)
  samplePrice = models.PositiveIntegerField(null=True)
  # [Enforcing minimum and maximum values in django model fields](http://blog.p3infotech.in/2013/enforcing-minimum-and-maximum-values-in-django-model-fields/)
  serviceQualityScore = models.PositiveIntegerField(null=True, validators=[MaxValueValidator(5)])
  serviceQualityReview = models.TextField(null=True)
  canShipToChina = models.BooleanField(default=True)
  moqForCustomDesign = models.PositiveIntegerField(null=True)

  def __unicode__(self):
    return "%s, %s" % (self.product.internalId, self.url)

import datetime
from apps.users.models import User 
class Order(TenantMixin):
  provider = models.ForeignKey(Provider)
  orderPrice = models.DecimalField("Price (provider change price frequently)", max_digits=5, decimal_places=2, blank=True, null=False, validators=[MinValueValidator(Decimal('0.01'))])
  shippingCostInChina = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=False)
  shippingCostToIndonesia = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=False)
  trackingNumber = models.CharField(max_length=255)
  # [cannot use datetime in createview]http://stackoverflow.com/questions/24137457/fielderror-at-admin-unknown-fields-added-on-specified-for-userprofile
  # [editable is needed](http://stackoverflow.com/questions/5499315/form-fields-for-modelform-with-inherited-models)
  orderingDate = models.DateTimeField("OrderingDate: yyyy-mm-dd HH:SS:MM")
  receivingDate = models.DateTimeField("ReceivingDate: yyyy-mm-dd HH:SS:MM", default='2099-12-31', null=True)
  # need to use related_name 
  # [](http://stackoverflow.com/questions/22538563/django-reverse-accessors-for-foreign-keys-clashing)
  # [](https://docs.djangoproject.com/en/1.6/ref/models/fields/#django.db.models.ForeignKey.related_name)
  orderMaker = models.ForeignKey(User, related_name='%(class)s_orderMaker')
  receiver = models.ForeignKey(User, related_name='%(class)s_receiver')
  orderQuantity = models.PositiveIntegerField()

class Ad(TenantMixin):
  url = models.URLField('Provider url', max_length=255)
  product = models.ForeignKey(Product)
  creator = models.ForeignKey(User)
  comment = models.TextField(default=" ", null=True)
