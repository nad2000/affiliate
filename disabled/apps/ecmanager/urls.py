from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from util import RequestStaff
import views

urlpatterns = patterns('',
  # url(r'^CreateProduct$', RequestStaff()(views.CreateProduct.as_view()), name='CreateProduct'),
  # url(r'^ListProducts$', RequestStaff()(views.ListProducts.as_view()), name='ListProducts'),
  url(r'^CreateProvider/$', RequestStaff()(views.CreateProvider.as_view()), name='CreateProvider'),
  url(r'^ListProviders/$', RequestStaff()(views.ListProviders.as_view()), name='ListProviders'),
  url(r'^UpdateProvider/(?P<pk>[0-9]+)/$', RequestStaff()(views.UpdateProvider.as_view()), name='UpdateProvider'),

  url(r'^CreateOrder/$', RequestStaff()(views.CreateOrder.as_view()), name='CreateOrder'),
  url(r'^ListOrders/$', RequestStaff()(views.ListOrders.as_view()), name='ListOrders'),
  url(r'^UpdateOrder/(?P<pk>[0-9]+)/$', RequestStaff()(views.UpdateOrder.as_view()), name='UpdateOrder'),
  url(r'^DeleteOrder/(?P<pk>[0-9]+)/$', RequestStaff()(views.DeleteOrder.as_view()), name='DeleteOrder'),

  url(r'^CreateAd/$', RequestStaff()(views.CreateAd.as_view()), name='CreateAd'),
  url(r'^ListAds/$', RequestStaff()(views.ListAds.as_view()), name='ListAds'),
  url(r'^UpdateAd/(?P<pk>[0-9]+)/$', RequestStaff()(views.UpdateAd.as_view()), name='UpdateAd'),
)
