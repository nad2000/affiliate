# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from decimal import Decimal
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('products', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Ad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField(max_length=255, verbose_name=b'Provider url')),
                ('comment', models.TextField(default=b' ', null=True)),
                ('creator', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('product', models.ForeignKey(to='products.Product')),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('orderPrice', models.DecimalField(blank=True, verbose_name=b'Price (provider change price frequently)', max_digits=5, decimal_places=2, validators=[django.core.validators.MinValueValidator(Decimal('0.01'))])),
                ('shippingCostInChina', models.DecimalField(max_digits=5, decimal_places=2, blank=True)),
                ('shippingCostToIndonesia', models.DecimalField(max_digits=5, decimal_places=2, blank=True)),
                ('trackingNumber', models.CharField(max_length=255)),
                ('orderingDate', models.DateTimeField(verbose_name=b'OrderingDate: mm/dd/yyyy')),
                ('receivingDate', models.DateTimeField(verbose_name=b'ReceivingDate: mm/dd/yyyy')),
                ('orderQuantity', models.PositiveIntegerField()),
                ('orderMaker', models.ForeignKey(related_name='order_orderMaker', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Provider',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField(max_length=255, verbose_name=b'Provider url')),
                ('productPrice', models.DecimalField(max_digits=5, decimal_places=2, blank=True)),
                ('moq', models.PositiveIntegerField()),
                ('samplePrice', models.PositiveIntegerField(null=True)),
                ('serviceQualityScore', models.PositiveIntegerField(null=True, validators=[django.core.validators.MaxValueValidator(5)])),
                ('serviceQualityReview', models.TextField(null=True)),
                ('canShipToChina', models.BooleanField(default=True)),
                ('moqForCustomDesign', models.PositiveIntegerField(null=True)),
                ('product', models.ForeignKey(to='products.Product', null=True)),
            ],
        ),
        migrations.AddField(
            model_name='order',
            name='provider',
            field=models.ForeignKey(to='ecmanager.Provider'),
        ),
        migrations.AddField(
            model_name='order',
            name='receiver',
            field=models.ForeignKey(related_name='order_receiver', to=settings.AUTH_USER_MODEL),
        ),
    ]
