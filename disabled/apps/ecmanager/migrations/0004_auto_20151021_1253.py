# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django_tenants.postgresql_backend.base


class Migration(migrations.Migration):

    dependencies = [
        ('ecmanager', '0003_auto_20151019_2343'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='status',
        ),
        migrations.AddField(
            model_name='ad',
            name='schema_name',
            field=models.CharField(default=b'dummy_schema', unique=True, max_length=63, validators=[django_tenants.postgresql_backend.base._check_schema_name]),
        ),
        migrations.AddField(
            model_name='order',
            name='schema_name',
            field=models.CharField(default=b'dummy_schema', unique=True, max_length=63, validators=[django_tenants.postgresql_backend.base._check_schema_name]),
        ),
        migrations.AddField(
            model_name='provider',
            name='schema_name',
            field=models.CharField(default=b'dummy_schema', unique=True, max_length=63, validators=[django_tenants.postgresql_backend.base._check_schema_name]),
        ),
    ]
