# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecmanager', '0002_auto_20151019_2337'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(default=1, max_length=100, choices=[(b'ordered', b'ordered'), (b'arrived at China aggregation serivce', b'arrived at China aggregation serivce'), (b'leaved China', b'leaved China'), (b'arrived In Indonesia', b'arrived In Indonesia')]),
        ),
    ]
