# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django_tenants.postgresql_backend.base


class Migration(migrations.Migration):

    dependencies = [
        ('ecmanager', '0004_auto_20151021_1253'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ad',
            name='schema_name',
            field=models.CharField(unique=True, max_length=63, validators=[django_tenants.postgresql_backend.base._check_schema_name]),
        ),
        migrations.AlterField(
            model_name='order',
            name='schema_name',
            field=models.CharField(unique=True, max_length=63, validators=[django_tenants.postgresql_backend.base._check_schema_name]),
        ),
        migrations.AlterField(
            model_name='provider',
            name='schema_name',
            field=models.CharField(unique=True, max_length=63, validators=[django_tenants.postgresql_backend.base._check_schema_name]),
        ),
    ]
