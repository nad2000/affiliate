# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecmanager', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='status',
            field=models.IntegerField(default=1, choices=[(1, b'ordered'), (2, b'arrived at China aggregation serivce'), (3, b'leaved China'), (4, b'arrived In Indonesia')]),
        ),
        migrations.AlterField(
            model_name='order',
            name='orderingDate',
            field=models.DateTimeField(verbose_name=b'OrderingDate: yyyy-mm-dd HH:SS:MM'),
        ),
        migrations.AlterField(
            model_name='order',
            name='receivingDate',
            field=models.DateTimeField(default=b'2099-12-31', null=True, verbose_name=b'ReceivingDate: yyyy-mm-dd HH:SS:MM'),
        ),
    ]
