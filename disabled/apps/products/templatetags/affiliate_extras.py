# [create tag](https://docs.djangoproject.com/en/1.8/howto/custom-template-tags/)
from django import template
register = template.Library()

from keyczar import keyczar
import logging
logger = logging.getLogger(__name__)
import coloredlogs
coloredlogs.install(level=logging.DEBUG)

@register.filter
def GetAid(userId, productId):
  s = 'userId=%s,productId=%s' % (userId, productId)
  location = 'config/keyczarkey'
  crypter = keyczar.Crypter.Read(location)
  s_encrypted = crypter.Encrypt(s)

  logger.debug("userId: %s" % userId)
  logger.debug("productId: %s" % productId)
  logger.debug("s_encrypted: %s" % s_encrypted)
  return s_encrypted
