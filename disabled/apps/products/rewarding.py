from django.http import HttpResponse, HttpResponseRedirect

import pytz
from datetime import date, timedelta
from .order_monitor import OrderMonitor

import logging
logger = logging.getLogger(__name__)
import coloredlogs
coloredlogs.install(level=logging.DEBUG)

def GetTimeOfCity(time, city = 'Asia/Tokyo'):
  import datetime 
  import pytz
  from pytz import timezone
  cityTimezone = timezone(city)
  cityTime = time.astimezone(cityTimezone)
  return cityTime

class Rewarding():
  SCOPES = 'https://www.googleapis.com/auth/gmail.readonly'
  CLIENT_SECRET_FILE = 'client_secrets.json'
  APPLICATION_NAME = 'Gmail API Python Quickstart'

  @classmethod
  def Reward(cls, request):
    from datetime import timedelta
    threshold = timedelta(minutes=7)

    newOrders = OrderMonitor.GetNewOrdersFromGmail()
    visitorStatuses = cls.GetVisitorStatuses()

    p = 0
    for visitorStatus in visitorStatuses:
      logger.debug("==============new visitorStatus ================")
      affilitor = visitorStatus.user
      while(p < len(newOrders)):
        logger.debug("-----------------next order------------")
        newOrder = newOrders[p] 
        p += 1
        logger.debug("visitorStatus.time: %s" % visitorStatus.time)
        logger.debug("newOrder['orderDateTime']: %s" % newOrder['orderDateTime'])
        logger.debug("newOrder['orderDateTime'] - visitorStatus.time: %s" % (newOrder['orderDateTime'] - visitorStatus.time))
        if newOrder['orderDateTime'] < visitorStatus.time:
          logger.debug("order is made before visiting. Not matching")
          continue
        if newOrder['orderDateTime'] - visitorStatus.time < threshold:
          visitorStatus.isOrder = True
          visitorStatus.orderMailId = newOrder['orderMailId']
          visitorStatus.orderMailDate = newOrder['orderDateTime']
          logger.debug("matched.")
          visitorStatus.save()
          affilitor.GetRewarded()
        else:
          logger.debug("order is made too late than visiting. Not matching.")
          break

    html = cls.FormOrderHtml(newOrders)
    return HttpResponse(html)

  @classmethod
  def FormOrderHtml(cls, orders):
    html = 'orders today'
    for order in orders:
      s = '%s, %s, %s' % (order['orderMailId'], order['orderDateTime'].strftime('%Y-%m-%d %H:%M:%S'), order['orderProductUrl'])
      html += '<p>' + s + '</p>'
    return html

  @classmethod
  def GetVisitorStatuses(cls):
    # When test this script outside django, need to set ev DJANGO_SETTINGS_MODULE 
    # [](http://jingyan.baidu.com/article/d2b1d1027508d15c7e37d4d2.html)
    from apps.products.models import VisitorStatus

    import peewee
    from config.private_settings import DB_PASSWORD 
    db = peewee.MySQLDatabase("affiliate", host="localhost", user="root", passwd=DB_PASSWORD)
    db.connect()

    #[](http://stackoverflow.com/questions/7217811/query-datetime-by-todays-date-in-django#comment25625205_7217923)
    import datetime
    today_min = datetime.datetime.combine(datetime.date.today(), datetime.time.min)
    today_max = datetime.datetime.combine(datetime.date.today(), datetime.time.max)
    visitorStatuses = VisitorStatus.objects.filter(time__range=(today_min, today_max)).order_by('time')

    # ''' 
    # Even though date is shown in the time zone specified in settings when show in browser, 
    # it is not processed in code. Need to manually change timezone.
    # ''' 
    for vs in visitorStatuses:
      vs.time = GetTimeOfCity(vs.time, 'Asia/Tokyo')
    return visitorStatuses 

if __name__ == '__main__':
  import os
  os.environ["DJANGO_SETTINGS_MODULE"] = 'config.settings' 
  main()
