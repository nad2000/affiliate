# -*- coding: utf-8 -*-
from django.views.generic import ListView
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from .models import Product, VisitorStatus
from django.http import HttpResponse, HttpResponseRedirect
#from apps.partner.models import Affiliate

from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy

from django.contrib.auth.decorators import permission_required
# Seems that there are two ways to turn a model class into form

import logging
logger = logging.getLogger(__name__)
import coloredlogs
coloredlogs.install(level=logging.DEBUG)

class CreateProduct(CreateView):
	model = Product
	fields = ['internalId', 'url', 'title', 'retailPrice', 'status']
	template_name = 'products/product_form.html'
	success_url = '/products/ListProducts'

	def form_valid(self, form):
		# Called when validation is passed
		# [](http://stackoverflow.com/questions/17826778/django-createview-is-not-saving-object)
		# [get field from form](http://stackoverflow.com/questions/4308527/django-model-form-object-has-no-attribute-cleaned-data)
		form.instance.url= form.cleaned_data['url']
		form.save()
		return super(CreateProduct, self).form_valid(form)

class ListProducts(ListView):
  template_name = "products/list.html"
  model = Product
  context_object_name = 'products'

class UpdateProduct(UpdateView):
  model = Product
  template_name = 'update.html'
  fields = CreateProduct.fields
  success_url = '/products/ListProducts'

class DeleteProduct(DeleteView):
  model = Product
  template_name = 'confirm_delete.html'
  success_url = '/products/ListProducts'

def VisitProduct(request, myAffiliateId): 
  ret = ParseAid(myAffiliateId)
  if not ret:
    return HttpResponse('incorrect affiliate link')

  (userId, productId) = ret

  from apps.users.models import User
  from apps.products.models import Product
  visitorStatus = VisitorStatus(
          user_id = userId, 
          product_id = productId, 
          ip=GetIp(request))
  visitorStatus.save()

  product = Product.objects.get(id=productId)
  return HttpResponseRedirect(product.url)

def GetIp(request):
  from ipware.ip import get_ip
  ip = get_ip(request)
  if ip is not None:
    return ip
  else:
    return '0.0.0.0'

def ParseAid(myAffiliateId):
  from keyczar import keyczar
  location = '/tmp/kz'
  crypter = keyczar.Crypter.Read(location)
  s_decrypted = crypter.Decrypt(myAffiliateId)
 
  import re
  m = re.search('userId=(\d+),productId=(\d+)', s_decrypted)
  if not m:
    logger.debug("myAffiliateId: %s" % myAffiliateId)
    return None
  return (m.group(1), m.group(2))

class ListVisitorStatus(ListView):
  template_name = "products/visitor_status.html"
  context_object_name = 'visitorStatuses'
  def get_queryset(self):
    return VisitorStatus.objects.filter(user=self.request.user, product_id = self.kwargs['productId'])

class DeleteVisitorStatus(DeleteView):
  model = VisitorStatus
  def get_success_url(self):
    return self.kwargs['next']
