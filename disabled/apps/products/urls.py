#-*- coding: utf-8 -*-
from django.conf.urls import patterns, url
import views 
from rewarding import Rewarding
import order_monitor 
from django.contrib.auth.decorators import login_required
from util import RequestStaff

urlpatterns = patterns('',
  url(r'^ListProducts/$', login_required(views.ListProducts.as_view()), name='ListProducts'),
  url(r'^CreateProduct/$', RequestStaff()(views.CreateProduct.as_view()), name='CreateProduct'),
  url(r'UpdateProduct/(?P<pk>[0-9]+)/$', RequestStaff()(views.UpdateProduct.as_view()), name='UpdateProduct'),
  url(r'DeleteProduct/(?P<pk>[0-9]+)/$', RequestStaff()(views.DeleteProduct.as_view()), name='DeleteProduct'),

  url(r'^VisitProduct/(?P<myAffiliateId>.+)/$', views.VisitProduct),

  url(r'^ListVisitorStatus/(?P<productId>[0-9]+)/$', login_required(views.ListVisitorStatus.as_view()), name='ListVisitorStatus'),
  url(r'^VisitorStatus/Delete/(?P<pk>[0-9]+)/next=(?P<next>.+)/$', RequestStaff()(views.DeleteVisitorStatus.as_view()), name='DeleteVisitorStatus'),

  url(r'^Rewarding/$', Rewarding.Reward),
  #url(r'^Orders/$', Rewarding.List),
)
