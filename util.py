from django.http import HttpResponse
def RequestStaff(login_url='/users/signin/'):
  from django.contrib.auth.decorators import user_passes_test
  return user_passes_test(lambda u:u.is_staff, login_url)

def WaitAndRedirect(request, url, message):
  from django.template import Template
  template = Template( 
      '''
      {%% block extrahead %%}
          %s
          <script type="text/javascript">
              setTimeout(function() {
                  window.location.href = "%s";
              }, 2000);
          </script>
      {%% endblock %%}
      ''' % (message, url)
      )

  from django.template import RequestContext
  context = RequestContext(request, {})
  return HttpResponse(template.render(context))

