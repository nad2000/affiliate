#-*- coding: utf-8 -*-
from django.conf.urls import patterns, url

from apps.customers.views import CreateTenantView

urlpatterns = patterns('',
    url(r'^create/$', CreateTenantView.as_view(), name='create'),
)
