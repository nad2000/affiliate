#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Dump product metrics from database

'''
Goal:
  Output data of a certain product
Grammar exampe:
  python -m result_visualizer.product_dump --csv myproducts https://www.bukalapak.com/p/handphone/case-cover/duypd-jual-samsung-galaxy-s5-flip-case-pu-leather-bahan-kulit-sintesis-kualitas-tinggi-ringan-port-dan-tombol-mudah-diakses-saat-dipasang-design-elegan
'''

import os
import argparse
import csv
import cStringIO
import codecs
from apps.products.models import mysql_db, Product, Metrics

ROOT_DIR = os.path.abspath(os.path.dirname(__file__))
TIME_FORMAT  = '%m/%d/%Y %H:%M:%S'

class UnicodeWriter:

  def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
    self.queue = cStringIO.StringIO()
    self.writer = csv.writer(self.queue, **kwds)
    self.stream = f
    self.encoder = codecs.getincrementalencoder(encoding)()

  def writerow(self, row):
    self.writer.writerow(map(lambda s: s.encode("utf-8"), row))
    data = self.queue.getvalue()
    data = data.decode("utf-8")
    data = self.encoder.encode(data)
    self.stream.write(data)
    self.queue.truncate(0)

def write_csv(filename, data):
  with open(filename, 'wb') as f:
    writer = UnicodeWriter(f, quoting=csv.QUOTE_ALL)
    for line in data:
      writer.writerow(line)

def pretty_print(data):
  for line in data:
    print ' | '.join(map(lambda s: s.ljust(20), line))

def get_product_metrics(product_url):
  data = []
  mysql_db.connect()
  try:
    product = Product.objects.filter(url=product_url.split('?')[0])
    for m in product.metrics.order_by(Metrics.timestamp):
      data.append(
        [
          m.timestamp.strftime(TIME_FORMAT),
          str(m.price),
          str(m.likes),
          str(m.pageviews),
        ]
      )
  except Product.DoesNotExist:
    print 'Product with a given URL cannot be found :: %s' % product_url
  finally:
    mysql_db.close()
    return data

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Product::Stats')
  parser.add_argument('product_url')
  parser.add_argument(
    '--csv',
    help='Write output into .csv file with a given name'
  )
  parser.add_argument(
    '--dir',
    help='Save .csv file in this directory (default: "output")',
    default='output',
  )
  args = parser.parse_args()
  product_url = args.product_url
  print 'Collecting metrics for %s' % product_url
  metrics = get_product_metrics(product_url)
  print 'Metrics found: %s' % len(metrics)
  if metrics:
    metrics.insert(0, ['Timestamp', 'Price', 'Likes', 'PageViews'])
    if args.csv:
      csv_file = args.csv if args.csv.endswith('.csv') else args.csv + '.csv'
      dir = os.path.join(ROOT_DIR, args.dir)
      if not os.path.exists(dir):
        os.mkdir(dir)
      filename = os.path.join(dir, csv_file)
      print 'All metrics saved to %s' % filename
      write_csv(filename, metrics)
    else:
      print '-' * 80
      pretty_print(metrics)
