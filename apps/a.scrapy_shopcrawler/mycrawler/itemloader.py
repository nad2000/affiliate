# -*- coding: utf-8 -*-

from scrapy.loader import ItemLoader, arg_to_iter

class DefaultInputProcessor(object):
  def __call__(self, value, loader_context=None):
    values = arg_to_iter(value)
    next_values = []
    for v in values:
      if isinstance(v, unicode):
        v = unicode.strip(v)
        if not v:
          continue
      next_values.append(v)
    return next_values

class DefaultOutputProcessor(object):
  def __call__(self, values, loader_context=None):
    return values

class DefaultItemLoader(ItemLoader):
  '''
  generate an item using self._values

  [](http://scrapy-chs.readthedocs.org/zh_CN/latest/topics/selectors.html)
  It accepts three arguments: item, selector and response. 
  selector is a html item tree and can extract certain items using xpath/css-selector.
  response seems to be a container of a selector and some extra data.
  If selector is provided, response is ignored. Otherwise, response creates a selector 
  '''
  default_input_processor = DefaultInputProcessor()

  def load_item(self):
    item = self.item
    for field_name in tuple(self._values):
      values = self.get_output_value(field_name)
      if (
        isinstance(values, (list, tuple)) and values and
        item.fields[field_name].get('take_first') != False
      ):
        first = None
        for v in values:
          if v is not None and v != '':
            first = v
            break
        values = first
      if (
        values is not None and
        (not hasattr(values, '__iter__') or values)
      ):
        item[field_name] = values
    for field_name, field_meta in item.fields.iteritems():
      if field_name not in item and 'default' in field_meta:
        default = field_meta['default']
        if getattr(default, '__call__', None):
          default = default()
        item[field_name] = default
    return item
