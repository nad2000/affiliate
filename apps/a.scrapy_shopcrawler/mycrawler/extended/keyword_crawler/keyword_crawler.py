#!/usr/bin/python
#
# Copyright 2015 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__author__ = ('security.xling@gmail.com' 'Ling Xu')

from scrapy import Request, Spider
from urllib import quote
from mycrawler.keyword_crawler.google_ad_api import GoogleAdApi
from mycrawler.keyword_crawler.related_keyword_finder import RelatedKeywordFinder
import time
import random
from apps.products.models import mysql_db, Keywords
from mycrawler.items import KeywordItem

import logging.config
logging.config.fileConfig('/home/xling/base/documents/scripts/programming/crossplatform/python/logging.conf')
import logging
logger = logging.getLogger(__name__)

class KeywordCrawler():

  def CrawlKeywords(self):
    '''
    (1) Get google search info (search search volume, cpc, ...) for keywords in DB.
    (2) Get the the seller number of these keywords in lazada.
    Save (1) and (2) into DB.
    '''
    keywords = [Keyword.keyword for Keyword in Keywords.filter(Keywords.searchVolume == -1)]
    keywords = list(set(keywords))
    assert len(keywords)

    split = 100
    logger.debug('keywords: ' + str(keywords))
    for i in range(len(keywords)/split):
      _keywords = keywords[i*split:(i+1)*split]
      logger.debug('_keywords:' + str(_keywords))
      keywordItems = GoogleAdApi.GetAnalyticDataForKeywords(_keywords)
      logger.debug('keywordItems:' + str(keywordItems))
      assert(len(keywordItems))
      for request in self.AppendSellerNumberAndWriteToDb(keywordItems):
        yield request

  def CorrectProductNumber(self):
    def GetItems(_keywords):
      assert len(_keywords)

      keywordItems = []
      for keyword in _keywords:
        keywordEntries = Keywords.filter(keyword = keyword)
        if keywordEntries.count() != 1:
          logger.debug ("keyword: %s" % keyword)
          logger.debug ("keywordEntries.count(): %s" % keywordEntries.count())
          logger.debug ("keywordEntries: %s" % str(keywordEntries))
          assert keywordEntries.count() == 1 

        keywordItem = KeywordItem()
        keywordItem['keyword'] = keywordEntries[0].keyword
        keywordItem['seedKeyword'] = keywordEntries[0].seedKeyword
        keywordItem['seedExpander'] = keywordEntries[0].seedExpander
        keywordItem['competition'] = keywordEntries[0].competition
        keywordItem['averageCpc'] = keywordEntries[0].averageCpc
        keywordItem['searchVolume'] = keywordEntries[0].searchVolume

        keywordItems.append(keywordItem)
      return keywordItems

    keywords = [Keyword.keyword for Keyword in Keywords.filter(Keywords.searchVolume != -1, Keywords.productNumber <= 36)]
    keywords = list(set(keywords))
    assert len(keywords)

    split = 100
    for i in range(len(keywords)/split):
      _keywords = keywords[i*split:(i+1)*split]
      assert(len(_keywords))
      keywordItems = GetItems(_keywords)
      assert(len(keywordItems))
      for request in self.AppendSellerNumberAndWriteToDb(keywordItems):
        print ("request: %s" % request)
        yield request

  def AppendSellerNumberAndWriteToDb(self, keywordItems):
    assert type(keywordItems) == type([]), type(keywordItems)
    assert len(keywordItems)

    for keywordItem in keywordItems:
      searchEngine = 'lazada'
      url = self.GetUrl(searchEngine, keywordItem['keyword'].split())

      yield Request(
        url,
        callback=self.parse_keyword,
        meta={ 'data': {'keywordItem': keywordItem} }
      )

  def GetUrl(self, searchEngine, keywords):
    if searchEngine == 'Google':
      url = 'https://www.google.co.jp/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q='
      sss = 'site:www.bukalapak.com '
      sss += ' '.join(['intitle:%s' % kw for kw in keywords])
      url += quote(sss)
    elif searchEngine == 'bukalapak':
      url = 'https://www.bukalapak.com/products?utf8=%E2%9C%93&search%5Bkeywords%5D='
      url += '+'.join(keywords)
    elif searchEngine == 'lazada':
      url = 'http://www.lazada.co.id/catalog/?q=' + '+'.join(keywords)
    else:
      assert 0, 'incorrect search engine'
    return url

  def parse_keyword(self, response):
    keywordItem = response.meta['data']['keywordItem']
    keywordItem['productNumber']= self.GetLazadaProductNumber(response)
    logger.debug('parse_keyword: ' + str(keywordItem))
    yield keywordItem

  def GetBukalapakProductNumber(self, response):
    lastPageXPath = "/html/body/div[1]/div[3]/div/div[1]/div/div[2]/div[1]/span[3]/text()"
    e = response.xpath(lastPageXPath)
    if(len(e)):
      # link to the last page is shown.
      pageNumber = e[0].extract()
    else:
      # if number of result pages is <=7, no link to last page is shown. 
      for i in reversed(range(1,7)):
        nextPathXPath = "/html/body/div[1]/div[3]/div/div[1]/div/div[2]/div[1]/a[%s]/text()" % i
        e = response.xpath(nextPathXPath)
        if( len(e) and self.IsNumber(e[0].extract()) ):
          pageNumber = e[0].extract()
          break
      else:
        pageNumber = 1
    return int(pageNumber) * 24

  def GetLazadaProductNumber(self, response):
    '''
    For kamera slr
      lastPageXPath = /html/body/div[2]/div[2]/div[2]/div/span/span[2]/span[1]/a[6]
    '''
    lastPageXPath = '/html/body/div[2]/div[2]/div[2]/div/span/span[2]/span[1]/a[6]/text()'
    e = response.xpath(lastPageXPath)
    if(len(e)):
      # link to the last page is shown.
      pageNumber = e[0].extract()
    else:
      # if number of result pages is <=7, no link to last page is shown. 
      for i in reversed(range(1,7)):
        nextPathXPath = '/html/body/div[2]/div[2]/div[2]/div/span/span[2]/span[1]/a[%s]/text()' % i
        e = response.xpath(nextPathXPath)
        if( len(e) and self.IsNumber(e[0].extract()) ):
          pageNumber = e[0].extract()
          break
      else:
        pageNumber = 1
    return int(pageNumber) * 36

  def IsNumber(self, s):
    try:
      int(s)
    except:
      return False
    return True
