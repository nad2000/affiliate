from items import KeywordItem
from googleads import adwords
from db import mysql_db, Keywords
from mycrawler.private_settings import NumberOfKeywordToGeneratePerSeedKeyword
client = adwords.AdWordsClient.LoadFromStorage('googleads.yaml')

import logging.config
logging.config.fileConfig('/home/xling/base/documents/scripts/crossplatform/python/logging.conf')
import logging
logger = logging.getLogger(__name__)

class GoogleAdApi():
  @classmethod
  def Test(cls):
    keywords = cls.GenerateRelatedKeywords('iphone case')
    print ("keywords: %s" % keywords)
    keywordItems = cls.GetAnalyticDataForKeywords(keywords)

  @classmethod
  def GenerateRelatedKeywords(cls, seedKeyword):
    assert type(seedKeyword) == type('')

    service = client.GetService('TargetingIdeaService', version='v201506')
    result = service.get( cls.FormTargetingIdeaSelector( [seedKeyword], dataToObtain = ['KEYWORD_TEXT'], requestType = 'IDEAS') )
    
    relatedKeywords = set()
    if 'entries' not in result:
      logger.warning('entries' not in result)
      logger.warning("seedKeyword: %s" % seedKeyword)
      logger.warning("result: %s" % result)
      return []

    for entry in result['entries']:
      data = entry['data']
      keyword = data[0]
      try:
        _keyword = keyword['value']['value'].encode("utf-8")
        print ("_keyword: %s" % _keyword)
      except: 
        print 'something is wrong'
        continue

      relatedKeywords.add(_keyword)

    return list(relatedKeywords)

  @classmethod
  def GetAnalyticDataForKeywords( cls, keywords ):
    assert type(keywords) == type([])
    keywords = list(set(keywords))
    assert len(keywords)
    service = client.GetService('TargetingIdeaService', version='v201506')

    selector =  cls.FormTargetingIdeaSelector( keywords, ['KEYWORD_TEXT', 'COMPETITION', 'SEARCH_VOLUME', 'AVERAGE_CPC'], requestType = 'STATS') 
    result = service.get(selector)
    
    if 'entries' not in result:
      logger.warning("'entries' not in result")
      logger.warning('result: ' + str(result))
      return []

    keywordItems = cls.FillHighTrafficItems(result['entries'], keywords)

    highTrafficKeywords = [keywordItem['keyword'] for keywordItem in keywordItems]
    cls.ExcludeLowTrafficItems(list(set(keywords) - set(highTrafficKeywords)))

    return keywordItems

  @classmethod
  def FillHighTrafficItems(cls, entries, keywords):
    keywordItems = []
    for entry in entries:
      if 'data' not in entry:
        logger.warning(entry)
        logger.warning(dir(entry))
        logger.warning(keywords)
      data = entry['data']

      keyword = data[0]
      competition = data[1]
      averageCpc = data[2]
      searchVolume = data[3]

      try: _keyword = keyword['value']['value'].encode("utf-8")
      except: continue
      try: _competition = competition['value']['value']
      except: continue
      try: _averageCpc = int(averageCpc['value']['value']['microAmount']/1000000)
      except: continue
      try: _searchVolume = searchVolume['value']['value']
      except: continue

      keywordEntries = Keywords.filter(keyword = _keyword)
      if keywordEntries.count() != 1:
        logger.debug ("data: %s" % data)
        logger.debug ("keywords: %s" % keywords)
        logger.debug ("_keyword: %s" % _keyword)
        logger.debug ("keywordEntries.count(): %s" % keywordEntries.count())
        logger.debug ("keywordEntries: %s" % keywordEntries)
        assert keywordEntries.count() == 1 

      keywordItem = KeywordItem()
      keywordItem['keyword'] = keywordEntries[0].keyword
      keywordItem['seedKeyword'] = keywordEntries[0].seedKeyword
      keywordItem['seedExpander'] = keywordEntries[0].seedExpander

      keywordItem['competition'] = _competition
      keywordItem['averageCpc'] = _averageCpc
      keywordItem['searchVolume'] = _searchVolume
      keywordItems.append(keywordItem)
    return keywordItems

  @classmethod
  def ExcludeLowTrafficItems(cls, keywords):
    '''
    These keywords have no data in google adwords.
    Set their searchVolume to -2 to avoid scanning them again.
    '''
    for keyword in keywords:
      keywordEntries = Keywords.objects.filter(Keywords.keyword == keyword)
      assert keywordEntries.count() == 1 

      keywordEntries[0].competition = -1
      keywordEntries[0].averageCpc = -1
      keywordEntries[0].searchVolume = -2
      keywordEntries[0].productNumber = -1
      rowsChanged = keywordEntries[0].save()
      assert rowsChanged == 1

  @classmethod
  def FormTargetingIdeaSelector(cls, keywords, dataToObtain, requestType = 'STATS'):
    '''
    keywords: e.g., ['iphone', 'smartphone']
    dataToObtain: e.g., ['KEYWORD_TEXT', 'COMPETITION', 'SEARCH_VOLUME', 'AVERAGE_CPC']
    '''
    assert type(keywords) == type([])
    assert len(keywords)
    paraTargetingIdeaSelector = {
        'searchParameters': 
          [
            {
              'xsi_type': 'LocationSearchParameter',
              'locations': [ {'id': 20436}, {'id': 20437}, {'id': 20439}, {'id': 20441}, {'id': 20443}, {'id': 20446}, {'id': 20450}, {'id': 20451}, {'id': 21291}, {'id': 9056637}, ], # indonesia cities
            },
            { 'xsi_type': 'LanguageSearchParameter', 'languages': [ {'id':1025}, ], },
            { 'xsi_type': 'RelatedToQuerySearchParameter', 'queries': keywords, },
          ],
        'ideaType': 'KEYWORD',
        'requestType':  requestType,
        'requestedAttributeTypes': dataToObtain,
        'paging': {
          'startIndex': str(0),
          'numberResults': str(NumberOfKeywordToGeneratePerSeedKeyword)
          },
        }
    return paraTargetingIdeaSelector 

if __name__ == '__main__':
  GoogleAdApi.Test()
