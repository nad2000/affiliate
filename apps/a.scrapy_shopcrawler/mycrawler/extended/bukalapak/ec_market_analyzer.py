from mycrawler.settings import DEBUG_MODE
from scrapy import Request
from mycrawler.itemloader import DefaultItemLoader
from mycrawler.items import ShopItem, MetricsItem
from apps.products.models import Product, Shop
from mycrawler.util import Util

class EcMarketAnalyzer:
  '''
  I need to ues scrapy logger that passed to this module. 
  Declear a dummy logger here to prevent pylint from complaining.
  '''
  logger = None 
  xpaths = {
    'nextProductPage': '//div[@class="pagination"]//a[@rel="next"]/@href',
    'shop_title': '//h1[contains(@class, "page-title")]//a/text()',
    'product_url': '//li[@class="product"]//h3//a/@href',
    'product_section': '//section[@itemscope]',
    'product_title': ['.//h1[@itemprop="name"]/text()'],
    'product_category': ['.//dd[@itemprop="category"]/text()'],
    'product_description': ['.//div[contains(@id, "product_desc")]//p/text()'],
    'product_price': (
      './/span[@itemprop="price"]'
      '//span[contains(@class, "amount")]/text()'
    ),
    #'product_stock': './/div[@class="product-stock"]/strong/text()',
    'product_pageviews':  ['//div[@class="product-stats"]//dd[@title="Dilihat"]/strong/text()'],
    'product_likes':  ['//div[@class="product-stats"]//dd[@title="Peminat"]/strong/text()'],
    'product_sales':  ['//div[@class="product-stats"]//dd[@title="Terjual"]/strong/text()'],
    'shopUrl': "//article[@class='user-display']//h5[@class='user__name']/a/@href",
    #'feedbackNumber': ["//a[@class='user-feedback-summary']/text()"],
  }
  rootUrl = 'https://www.bukalapak.com'
  xhr_url = 'https://www.bukalapak.com/products/%s/user_manage'
  xhr_headers = {'X-Requested-With': 'XMLHttpRequest'}
  crawlAllShops = False

  @classmethod
  def RecordProductsInShops(cls, logger, shopUrls):
    cls.logger = logger
    cls.logger.debug("RecordProductsInShops")
    for shopUrl in shopUrls:
      cls.logger.info('Processing shop :: %s' % shopUrl)
      yield cls.ToRecordProductsInShop(cls.logger, shopUrl)

  @classmethod
  def ToRecordProductsInShop(cls, logger, shopUrl):
    cls.logger = logger
    cls.logger.debug("ToRecordProductsInShop")
    url = shopUrl.rstrip('/') + '/products'
    return Request(
      url,
      callback=cls.__parseShopProducts,
      meta={
        'data': {
          'shopUrl': shopUrl,
          'page': 1,
        }
      },
    )
 
  @classmethod
  def __parseShopProducts(cls, response):
    cls.logger.debug("__parseShopProducts")

    try:
      if response.meta['data']['page'] == 1:
        cls._validate_url(response, 'shop')
      shopItem = response.meta['data'].get('shopItem')
    except Exception as e:
      import sys
      cls.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
      cls.logger.error(e.message)
      raise e

    
    shopReference = Shop.objects.filter(Shop.url == response.meta['data']['shopUrl'])
    # if shopItem is None:
      # shopItem = cls.LoadShopItemFromDb(response, response.meta['data']['shopUrl'])
      # cls.logger.debug("yield str(shopItem): %s" % str(shopItem))
      # yield shopItem
    # if not DEBUG_MODE and shopItem.db_entry is None:
      # cls.logger.debug("loading shopItem failed")
      # return
    # shopReference = shopItem['url'] if shopItem else shopItem.db_entry

    for request in cls.UpdateMetricsOfProductsOnCurrentShop(response, shopReference):
      yield request

    for request in cls.__RequestNextPageOfCurrentShop(response, shopReference):
      yield request

  @classmethod
  def UpdateMetricsOfProductsOnCurrentShop(cls, response, shopReference):
    for product_url in response.xpath(cls.xpaths['product_url']).extract():
      product_url = response.urljoin(product_url).split('?')[0] # remove query
      cls.logger.info('processing product :: %s' % product_url)

      yield Request(
        product_url,
        callback=cls.parse_Product,
        meta={
          'data': {
            'shopReference': shopReference
          },
        },
      )

  @classmethod
  def __RequestNextPageOfCurrentShop(cls, response, shopItem):
    cls.logger.debug("__RequestNextPageOfCurrentShop")
    next_page_url = response.xpath(cls.xpaths['nextProductPage']).extract_first()
    if next_page_url:
      yield Request(
        response.urljoin(next_page_url),
        callback=cls.__parseShopProducts,
        meta={
          'data': {
            'shopUrl': response.meta['data']['shopUrl'],
            'shopItem': shopItem,
            'page': response.meta['data']['page'] + 1,
          }
        },
      )

  @classmethod
  def parse_Product(cls, response):
    try:
      assert cls.logger
      cls.logger.debug("parse_Product")

      productUrl = cls._validate_url(response, 'product')
      product_section = response.xpath(cls.xpaths['product_section'])

      if not product_section:
        cls.logger.error( 'No product section on the page :: %s. return' % productUrl)
        return

      assert 'data' in response.meta
      assert 'shopReference' in response.meta['data']
      productReference = cls.__UpdateProduct(response, productUrl, shopReference = response.meta['data']['shopReference'])

      # _xpaths = {
              # 'product_title': xpaths['product_title'],
              # 'product_category': xpaths['product_category'],
              # 'product_description': xpaths['product_description'],
              # }
      # productReference = Util.UpdateProduct(
              # cls.logger, 
              # response, 
              # productUrl, 
              # response.meta['data']['shopReference'], 
              # _xpaths)

    except Exception as e:
      import sys
      cls.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
      cls.logger.error('str(e) %s' %  str(e))
      cls.logger.debug("response.url: %s" % response.url)
      cls.logger.error(e.message)
      raise e
    else:
      for request in cls.__GetMetricsItemForUpdate(response, productReference, product_section):
        yield request

  @classmethod
  def __UpdateProduct(cls, response, productUrl, shopReference):
    '''
    MetricsItem need its product exists in DB before it can be inserted into DB.
    This method directly writes Product into DB instead of throwing ProductItem into pipelines so that MetricsItem can be immediately inserted.
    '''
    cls.logger.debug("__UpdateProduct")
    from peewee import IntegrityError
    try:
      product = Product()
      products = Product().objects.filter(Product.url == productUrl) 
      if products.count() > 0: 
        product = products[0]

      # title returned from xpath usually contains \n and the head and tail.
      title = Util.GetValueByXpaths(cls.logger, response, cls.xpaths['product_title'])
      if title[0] == '\n': 
        title = title[1:]
      if title[-1] == '\n': 
        title = title[:-1]
      product.title = title

      product.category = Util.GetValueByXpaths(cls.logger, response, cls.xpaths['product_category'])
      product.description = Util.GetValueByXpaths(cls.logger, response, cls.xpaths['product_description'])
      product.url = productUrl
      if shopReference:
        product.shop = shopReference
      if not DEBUG_MODE:
        product.save()
      return product
    except IntegrityError, message:
      errorcode = message[0]  # get MySQL error code
      if errorcode == 1062:  # if duplicate 
        pass
    except Exception as e:
      import sys
      cls.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
      cls.logger.error('DBError :: %s' %  str(e))
      cls.logger.debug("productUrl: %s" % productUrl)
      cls.logger.error(e.message)
      raise e

  @classmethod
  def __GetMetricsItemForUpdate(cls, response, productReference, product_section):
    cls.logger.debug("__GetMetricsItemForUpdate")

    try:
      metrics_item = DefaultItemLoader(MetricsItem(), selector=product_section[0])
      metrics_item.add_xpath('price', cls.xpaths['product_price'])
      metrics_item.add_value( 'product', productReference)
      productUrl = cls._validate_url(response, 'product')
      metrics_url = cls.xhr_url % productUrl.rstrip('/').split('/')[-1]
      cls.logger.debug("yield Request metrics_item: %s" % metrics_url)
    except Exception as e:
      import sys
      cls.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
      cls.logger.error('str(e) %s' %  str(e))
      cls.logger.error(e.message)
      raise e
    else:
      yield Request(
          metrics_url,
          callback = cls.__parse_RecordMetrics,
          headers = cls.xhr_headers,
          meta={
            'data': {'metrics_item': metrics_item},
            },
          )

  @classmethod
  def _validate_url(cls, response, obj=''):
    # check if the real URL of loaded page differs from the requested one
    cls.logger.debug("_validate_url")
    
    request_url = response.request.url.split('?')[0]
    response_url = response.url.split('?')[0]
    if request_url != response_url:
      cls.logger.warning(
        '%s URL mismatch :: %s != %s' %
        (obj.title(), request_url, response_url)
      )
    return response_url

  @classmethod
  def __parse_RecordMetrics(cls, response):
    cls.logger.debug("__parse_RecordMetrics"  + (", response.url: %s" %  response.url))
    try:
      metrics_item = response.meta['data']['metrics_item']
      metrics_item.add_value('likes', Util.GetValueByXpaths(cls.logger, response,  cls.xpaths['product_likes'] ))
      metrics_item.add_value('pageviews', Util.GetValueByXpaths(cls.logger, response,  cls.xpaths['product_pageviews'] ))
      metrics_item.add_value('sales', Util.GetValueByXpaths(cls.logger, response,  cls.xpaths['product_sales'] ))
      metrics_item = metrics_item.load_item()
      cls.logger.debug("str(metrics_item): %s" % str(metrics_item))
    except Exception as e:
      import sys
      cls.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
      cls.logger.error(e.message)
      raise e
    else:
      yield metrics_item
