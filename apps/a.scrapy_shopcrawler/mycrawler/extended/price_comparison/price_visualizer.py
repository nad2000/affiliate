from apps.products.models import SearchKey, Product
from peewee import fn
from mycrawler.price_comparison.image_comparison import TryToDownloadImage

def CallBashAndGetResults(commandString):
  import subprocess as sp
  process = sp.Popen(commandString, shell = True, stdout = sp.PIPE, stderr = sp.PIPE)
  result, error = process.communicate()
  return result

imageRootDir = ''
from mycrawler.private_settings import imageRootDir
assert imageRootDir is not ''

def ComparePrice():
  for searchKeyEntry in SearchKey.select(fn.Distinct(SearchKey.searchKey)):
    # lProductEntries = Product.select().join(SearchKey).where(Product.url.contains('lazada'), SearchKey.searchKey == searchKeyEntry.searchKey)
    lProductEntries = Product.objects.filter(url__contains='lazada', searchkey_searchkey == searchKeyEntry.searchKey)
    # bProductEntries = Product.select().join(SearchKey).where(Product.url.contains('bukalapak'), SearchKey.searchKey == searchKeyEntry.searchKey)
    for bProductEntry in bProductEntries:
      urlsOfImagesToCompare = [lProductEntries[0].imagePath, bProductEntries[0].imagePath] 
      filePaths = TryToDownloadImage(imageRootDir, urlsOfImagesToCompare)
      cmd = 'pyssim %s %s' % (filePaths[0], filePaths[1])
      similarity = CallBashAndGetResults(cmd)
      print ("similarity: %s" % similarity)
      bProductEntry.similarity = similarity
      bProductEntry.save()
      
    #metricsEntries = Metrics.select(Metrics.Price).join(SearchKey).where(Metrics.product == Product, Product.url.contains('lazada'), Product == SearchKey.product, SearchKey.searchKey == searchKeyEntry.searchKey)
    #lPrice = metricsEntries[0].price
    
    #metricsEntries = Metrics.select(Metrics.Price).join(SearchKey).where(Metrics.product == Product, Product.url.contains('bukalapak'), Product == SearchKey.product, SearchKey.searchKey == searchKeyEntry.searchKey)
    #bPrice = sum([metricsEntry.price for metricsEntry in metricsEntries])/metricsEntries.count()

    #logger.debug("lPrice: %s" % lPrice  + (", \nbPrice: %s" % bPrice))
