# -*- coding: utf-8 -*-

#class DefaultItemPipeline(object):

#  def process_item(self, item, spider):
#    return item

# import logging.config
# logging.config.fileConfig('/home/xling/base/documents/scripts/crossplatform/python/logging.conf')
# import logging
# logger = logging.getLogger(__name__)

import logging
import coloredlogs
logger = logging.getLogger('your-module')
coloredlogs.install(level='DEBUG')

class MySQLPipeline(object):
  def process_item(self, item, spider):
    try:
      logger.debug("process_item: type(item): %s" % type(item))
      logger.debug("process_item: str(item): %s" % str(item))
      saveResult = item.save()
    except Exception as e:
      import sys, os
      logger.error(e.message)
      logger.error(e.__cause__.pgcode)
      raise e
