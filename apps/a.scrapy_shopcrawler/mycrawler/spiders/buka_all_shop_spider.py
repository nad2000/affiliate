from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from mycrawler.extended.bukalapak.ec_market_analyzer import EcMarketAnalyzer
from mycrawler.util import Util
from apps.products.models import Product

class BukaAllShopSpider(CrawlSpider):
  name = 'allShopSpider'
  allowed_domains = ['bukalapak.com']
  start_urls = [product.url for product in Product.objects.all()]
  rules=(
      Rule(
        LinkExtractor( allow = "https://www.bukalapak.com/p/.*"), 
        callback = "parse_product", 
        follow=True
        ),
      )

  def parse_product(self, response):
    self.logger.debug("parse_product")
    try:
      shopUrlXPaths = ["//article[@class='user-display']//h5[@class='user__name']/a/@href"]
      shop_url = Util.GetValueByXpaths(self.logger, response, shopUrlXPaths)
      shop_url = response.urljoin(shop_url)
      self.logger.debug("shop_url: %s" % shop_url)
    except Exception as e:
      import sys
      self.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
      self.logger.error('str(e) %s' %  str(e))
      self.logger.error(e.message)
      raise e
    else:
      yield EcMarketAnalyzer.ToRecordProductsInShop(self.logger, shop_url)
