# -*- coding: utf-8 -*-
from django.contrib.auth.models import User as _User, AbstractUser
from django.db import models
import datetime

class User(AbstractUser):
  USERNAME_FIELD = 'username'
  REQUIRED_FIELDS = ['email', 'award']

  award = models.IntegerField(default=0)

  def __unicode__(self):
    return self.username

  def GetRewarded(self):
    self.award += 10
    self.save()

class UserProfile(models.Model):
  user = models.OneToOneField(User)
  activation_key = models.CharField(max_length=40, blank=True)
  key_expires = models.DateTimeField(default=datetime.date.today() + datetime.timedelta(days=1))
    
  def __str__(self):
    return self.user.username

  class Meta:
    verbose_name_plural=u'User profiles'
