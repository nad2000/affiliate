# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from django.contrib.auth.views import login, logout
from django.views.generic import TemplateView
from util import RequestStaff
import views

urlpatterns = patterns('',
    url(r'^signup/$', views.RegisterUser, name='signup'),
    url(r'^confirm/(?P<activation_key>\w+)/', views.register_confirm),
    url(r'^signin/$', login, name='login', kwargs={"template_name": "users/login.html"}),
    url(r'^logout_confirm/$', TemplateView.as_view(template_name='users/logout.html'), name="logout_confirm"),
    url(r'^logout/$', logout, name='logout'),

    url(r'^ListUsers$', RequestStaff()(views.ListUsers.as_view()), name='ListUsers'),
    url(r'^UpdateUser/(?P<pk>[0-9]+)/$', RequestStaff()(views.UpdateUser.as_view()), name='UpdateUser'),
    url(r'^DeleteUser/(?P<pk>[0-9]+)/$', RequestStaff()(views.DeleteUser.as_view()), name='DeleteUser'),

    url(r'^EnableCurrentUserProductManagement/$', views.EnableCurrentUserProductManagement),
)
