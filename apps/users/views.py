# -*- coding: utf-8 -*-
from django.views.generic import CreateView
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.conf import settings
from django.http import HttpResponse
import util

from .forms import UserForm
from .models import User


MIN_REQUEST_AMOUNT = getattr(settings, 'AFFILIATE_MIN_BALANCE_FOR_REQUEST', 0)

from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import auth
from django.core.context_processors import csrf
from forms import *
from models import *
from django.template import RequestContext
from django.core.mail import send_mail
import hashlib, datetime, random
from django.utils import timezone

def RegisterUser(request):
  args = {}
  args.update(csrf(request))
  if request.method == 'POST':
    form = RegistrationForm(request.POST)
    args['form'] = form
    if form.is_valid(): 
      form.save()  # save user to database if form is valid

      username = form.cleaned_data['username']
      email = form.cleaned_data['email']
      salt = hashlib.sha1(str(random.random())).hexdigest()[:5]      
      activation_key = hashlib.sha1(salt+email).hexdigest()      
      key_expires = datetime.datetime.today() + datetime.timedelta(2)

      #Get user by username
      user=User.objects.get(username=username)

      # Create and save user profile                                                                  
      new_profile = UserProfile(user=user, activation_key=activation_key, key_expires=key_expires)
      new_profile.save()

      # Send email with activation key
      email_subject = 'Account confirmation'
      email_body = "Hey %s, thanks for signing up." % username
      email_body += "To activate your account, click this link within 48hours %s/users/confirm/%s" % (request.get_host(), activation_key)

      send_mail(email_subject, email_body, 'Frz6Jbf9@gmail.com', [email], fail_silently=False)

      return util.WaitAndRedirect(request, '/users/signup', 'pre-regitration succeeded.')    
  else:
    args['form'] = RegistrationForm()

  return render_to_response('users/register.html', args)

def register_confirm(request, activation_key):
  #check if user is already logged in and if he is redirect him to some other url, e.g. home
  if request.user.is_authenticated():
    HttpResponseRedirect('/home')

  # check if there is UserProfile which matches the activation key (if not then display 404)
  user_profile = get_object_or_404(UserProfile, activation_key=activation_key)

  #check if the activation key has expired, if it hase then render confirm_expired.html
  if user_profile.key_expires < timezone.now():
    #return render_to_response('user_profile/confirm_expired.html')
    return util.WaitAndRedirect(request, '/users/signup', 'confirmation expired. Please register again.')
  #if the key hasn't expired save user and set him as active and render some template to confirm activation
  user = user_profile.user
  user.is_active = True
  user.save()
  #return render_to_response('user_profile/confirm.html')
  return util.WaitAndRedirect(request, '/users/signin', 'mail confirmed. The registration is done.')

# [](https://docs.djangoproject.com/en/1.8/topics/class-based-views/generic-editing/)
from django.contrib.auth.decorators import login_required
@login_required
def EnableCurrentUserProductManagement(request):
  #SetPermission(request)
  # [](http://stackoverflow.com/questions/30683528/failed-to-to-turn-django-staff-flag-on-programatically)
  request.user.is_staff = True
  request.user.save()
  print ("EnableCurrentUserProductManagement")
  print ("request.user: %s" % request.user)
  print ("request.user.is_staff: %s" % request.user.is_staff)

  return util.WaitAndRedirect(request, 'assign permission succeeded')
  #return HttpResponse('assign permission succeeded')

def SetPermission(request):
  from django.contrib.auth.models import Group, Permission
  from django.contrib.contenttypes.models import ContentType
  from config import settings
  try:
    content_type = ContentType.objects.get(app_label='users', model='User')
  except ObjectDoesNotExist:
    return HttpResponse('something is wrong. cannot create content_type')

  adminPermission = Permission.objects.get_or_create(
      codename='can_create_product', 
      name='Can create product', 
      content_type=content_type)[0]
  #user = User.objects.get(username='leonexu')
  request.user.user_permissions.add(adminPermission)

# users
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
class ListUsers(ListView):
  from apps.users.models import User
  template_name = "users/list.html"
  model = User
  #queryset = MyAffiliate.objects.select_related('user')
  context_object_name = 'users'

class UpdateUser(UpdateView):
  model = User
  template_name = 'common/update.html'
  success_url = '/users/ListUsers'

class DeleteUser(DeleteView):
  model = User
  template_name = 'common/confirm_delete.html'
  success_url = '/users/ListUsers'
