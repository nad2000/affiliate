#-*- coding: utf-8 -*-
from django.conf.urls import patterns, url
import views 
from rewarding import Rewarding
import order_monitor 
from django.contrib.auth.decorators import login_required
from util import RequestStaff

urlpatterns = patterns('',
    url(r'^metrics/create/$', login_required(views.MetricFormView.as_view()), name='metrics-create'),
    url(r'^metrics/$', login_required(views.MetricsView.as_view()), name='metrics'),
    
    url(r'^create/$', RequestStaff()(views.CreateProduct.as_view()), name='create'),
    url(r'update/(?P<pk>.*\d+)/$', RequestStaff()(views.UpdateProduct.as_view()), name='update'),
    url(r'delete/(?P<pk>.*\d+)/$', RequestStaff()(views.DeleteProduct.as_view()), name='delete'),

    url(r'^VisitProduct/(?P<myAffiliateId>.+)/$', views.VisitProduct),

    url(r'^visitor-status/(?P<pk>.*\d+)/$', login_required(views.ListVisitorStatus.as_view()), name='visitor-status'),
    url(r'^VisitorStatus/Delete/(?P<pk>[0-9]+)/next=(?P<next>.+)/$', RequestStaff()(views.DeleteVisitorStatus.as_view()), name='DeleteVisitorStatus'),

    url(r'^Rewarding/$', Rewarding.Reward),
    #url(r'^Orders/$', Rewarding.List),
    url(r'^$', login_required(views.ListProducts.as_view()), name='list'),
)
