# -*- coding: utf-8 -*-
from django import forms

from .models import Product
from .models import ProductStatus


# class ProductForm(ModelForm):
  # class Meta:
    # model = Product
    # fields = ['url', 'title', 'price']

    # #[Select ForeignKey field in form ChoiceField](http://stackoverflow.com/questions/1674458/select-foreignkey-field-in-form-choicefield)
    # status = ModelChoiceField(queryset=ProductStatus.objects.all())


class ScrapyFormView(forms.Form):
    """
    Form that accepts scrapy commands
    """
    command = forms.CharField(max_length=500, required=True)

    def __init__(self, *args, **kwargs):
        super(ScrapyFormView, self).__init__(*args, **kwargs)
        self.fields["command"].widget.attrs['size'] = "100%"