from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Metrics


@receiver(post_save, sender=Metrics)
def post_save_order(sender, instance, created, *args, **kwargs):
    if created and instance.product:
        product = instance.product
        product.stock -= instance.sales
        product.save()
