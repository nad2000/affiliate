# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0010_remove_product_test1'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='test',
        ),
    ]
