# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0002_product_test'),
    ]

    operations = [
        migrations.AlterField(
            model_name='metrics',
            name='commentNumber',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='metrics',
            name='likes',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='metrics',
            name='pageviews',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='metrics',
            name='price',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='metrics',
            name='sales',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='metrics',
            name='score',
            field=models.FloatField(default=0, null=True),
        ),
        migrations.AlterField(
            model_name='metrics',
            name='timestamp',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
