# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0005_remove_metrics_score'),
    ]

    operations = [
        migrations.RenameField(
            model_name='metrics',
            old_name='score1',
            new_name='score',
        ),
    ]
