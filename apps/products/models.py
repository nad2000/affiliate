# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
#from relish.helpers import upload_to

class Shop(models.Model):
  title = models.CharField(max_length=255)
  url = models.URLField(unique=True, max_length=255)
  feedbackNumber = models.PositiveIntegerField()
  score = models.FloatField(default=0.0)
  lastVisited = models.DateTimeField(auto_now_add=True)
  class Meta: 
    # These models are also used by scrapy. Django automatically adds app label as the table prefix. We need to tell this prefix to scrapy.
    db_table = "products_shop"

class ProductStatus(models.Model):
  status = models.CharField(max_length=255, default='dummy product status', null=False)
  def __unicode__(self):
    return self.status
  class Meta: 
    db_table = "products_productstatus"

# insert default statuses
# when the first time manage.py migrate is called, products_productstatus table is not created yet. get_or_create() will fail
try: 
  ProductStatus.objects.get_or_create(status = '1_not_yet_processed')[0].save()
  ProductStatus.objects.get_or_create(status = '2_ordered_not_arrived')[0].save()
  ProductStatus.objects.get_or_create(status = '3_ordered_arrived')[0].save()
  ProductStatus.objects.get_or_create(status = '4_to_be_ordered')[0].save()
  ProductStatus.objects.get_or_create(status = '5_not_to_order_any_more (low profit)')[0].save()
  ProductStatus.objects.get_or_create(status = '6_not_to_order_any_more (no provider)')[0].save()
  ProductStatus.objects.get_or_create(status = '7_not_to_order_any_more (high purchase price)')[0].save()
except:
  pass

class Product(models.Model):
  """Very simple product"""
  internalId = models.PositiveIntegerField(null=True)
  url = models.URLField(unique=True, max_length=255)
  # this field is outdated. It is created for affiliate system only. Should be moved to Metric.
  retailPrice = models.PositiveIntegerField(_("Retail price"), default=999, null=False)
  title = models.CharField(null=False, max_length=255)
  category = models.CharField(max_length=255, default="dummy category")
  description = models.CharField(max_length=255, default="dummy description")
  imagePath = models.CharField(max_length=255, default='dummy image path')
  similarity = models.FloatField(default=0.0) # the simularity of this products on buka and lazada

  status = models.ForeignKey(ProductStatus, null=True)
  sellingUrls = models.URLField(null=True, max_length=255)
  stock = models.PositiveIntegerField(default=1, null=True)

  # One-To-Many relationship (shop has many products)
  # sometime, shop is not important. So, set null = True
  shop = models.ForeignKey(Shop, related_name='products', null = True)
  #searchKeys = models.ForeignKey(SearchKeys, related_name = 'products' )

  class Meta:
    verbose_name = _('Product')
    verbose_name_plural = _('Products')
    db_table = "products_product"

  def __unicode__(self):
    return "id:%s, internalId:%s, %s" % (self.id, self.internalId, self.title)

  def get_image_url(self):
    if self.image:
      return self.image.url
    else:
      return settings.DEFAULT_IMAGE

class SearchKey(models.Model):
  searchKey = models.CharField(max_length=255)
  subSearchKey = models.CharField(max_length=255)
  product = models.ForeignKey(Product)
  class Meta: 
    db_table = "products_searchkey"

# METRICS TABLE
class Metrics(models.Model):
  '''
  We trace the data of each product at different times. 
  So, each product has multiple metrics
  '''
  timestamp = models.DateTimeField(auto_now_add=True)
  pageviews = models.PositiveIntegerField(default=0, null=True)
  likes = models.PositiveIntegerField(default=0, null=True)
  price = models.PositiveIntegerField(default=0, null=True)
  sales = models.PositiveIntegerField(default=0, null=True)
  commentNumber = models.PositiveIntegerField(default=0, null=True) 
  score = models.FloatField(default=0.0, null=True)
  #stock = models.PositiveIntegerField(default=0)
  # One-To-Many relationship (product has many metrics)
  product = models.ForeignKey(Product, related_name='metrics')

  class Meta: 
    db_table = "products_metrics"

class Keywords(models.Model):
  # https://docs.djangoproject.com/en/1.8/ref/models/fields/
  # .save() always fails once I use primary_key = true. 
  # The reason is unclear.
  keyword = models.CharField(unique=True, max_length=255)
  seedKeyword = models.CharField(max_length=255)
  seedExpander = models.CharField(max_length=255)

  searchVolume = models.PositiveIntegerField()
  averageCpc = models.IntegerField()
  competition = models.FloatField()
  productNumber = models.PositiveIntegerField()

  class Meta: 
    db_table = "products_keywords"

  # https://docs.djangoproject.com/en/1.8/ref/models/fields/
  # .save() always fails once I use primary_key = true. 
  # The reason is unclear.

  def Init(self, _seedKeyword='', _seedExpander='', _keyword='', _searchVolume=-1, _averageCpc=-1, _competition=-1, _productNumber=-1):
    self.seedKeyword = _seedKeyword
    self.seedExpander = _seedExpander
    self.keyword = _keyword
    self.searchVolume = _searchVolume
    self.averageCpc = _averageCpc
    self.competition = _competition
    self.productNumber = _productNumber

from apps.users.models import User
class VisitorStatus(models.Model):
  user = models.ForeignKey(User)
  product = models.ForeignKey(Product)

  time = models.DateTimeField(auto_now_add=True)
  ip = models.GenericIPAddressField()

  isOrder = models.BooleanField(default=False)
  orderMailId = models.CharField(max_length=32, null=True)
  orderMailDate = models.DateTimeField(null=True)
