# -*- coding: utf-8 -*-
import os
        
from django.conf import settings
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import permission_required
from django.core.management import call_command
from django.core.urlresolvers import reverse_lazy, reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from django.views.generic import ListView
from django.views.generic.base import View
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView

from .forms import ScrapyFormView
from .models import Product, VisitorStatus, Metrics

#from apps.partner.models import Affiliate

# Seems that there are two ways to turn a model class into form

import logging
logger = logging.getLogger(__name__)
import coloredlogs
coloredlogs.install(level=logging.DEBUG)


class CreateProduct(CreateView):
    model = Product
    fields = ['internalId', 'url', 'title', 'retailPrice', 'status']
    template_name = 'common/create.html'
    success_url = '/products/'

    def form_valid(self, form):
        # Called when validation is passed
        # [](http://stackoverflow.com/questions/17826778/django-createview-is-not-saving-object)
        # [get field from form](http://stackoverflow.com/questions/4308527/django-model-form-object-has-no-attribute-cleaned-data)
        form.instance.url= form.cleaned_data['url']
        form.save()
        return super(CreateProduct, self).form_valid(form)


class MetricFormView(CreateView):

    model = Metrics
    fields = ["product", "sales"]
    template_name = "common/create.html"
    success_url = '/products/metrics/'


class ListProducts(ListView):
  template_name = "products/list.html"
  model = Product
  context_object_name = 'products'
  def get_queryset(self):
    return Product.objects.order_by('internalId')

class UpdateProduct(UpdateView):
  model = Product
  template_name = 'common/update.html'
  fields = CreateProduct.fields
  success_url = '/products/'

class DeleteProduct(DeleteView):
  model = Product
  template_name = 'common/confirm_delete.html'
  success_url = '/products/'

def VisitProduct(request, myAffiliateId): 
  ret = ParseAid(myAffiliateId)
  if not ret:
    return HttpResponse('incorrect affiliate link')

  (userId, productId) = ret

  from apps.users.models import User
  from apps.products.models import Product
  visitorStatus = VisitorStatus(
          user_id = userId, 
          product_id = productId, 
          ip=GetIp(request))
  visitorStatus.save()

  product = Product.objects.get(id=productId)
  return HttpResponseRedirect(product.url)

def GetIp(request):
  from ipware.ip import get_ip
  ip = get_ip(request)
  if ip is not None:
    return ip
  else:
    return '0.0.0.0'

def ParseAid(myAffiliateId):
  from keyczar import keyczar
  location = '/tmp/kz'
  crypter = keyczar.Crypter.Read(location)
  s_decrypted = crypter.Decrypt(myAffiliateId)
 
  import re
  m = re.search('userId=(\d+),productId=(\d+)', s_decrypted)
  if not m:
    logger.debug("myAffiliateId: %s" % myAffiliateId)
    return None
  return (m.group(1), m.group(2))

class ListVisitorStatus(ListView):
  template_name = "products/visitor_status.html"
  context_object_name = 'visitorStatuses'
  def get_queryset(self):
    return VisitorStatus.objects.filter(user=self.request.user, product_id = self.kwargs['productId'])

class DeleteVisitorStatus(DeleteView):
  model = VisitorStatus
  def get_success_url(self):
    return self.kwargs['next']


class AdminCrawlSpiderView(View):

    @method_decorator(staff_member_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AdminCrawlSpiderView, self).dispatch(request, *args, **kwargs)

    def get(self, request):
        scrapy_project = os.path.join(settings.BASE_DIR, 'scrapy_shopcrawler')
        spider = self.kwargs.get("spider")
        crawler_command = "%s %s" % ("scrapy crawl", spider)
        os.chdir(scrapy_project)
        os.system(crawler_command)
        return HttpResponseRedirect(reverse("products:list"))


class AdminScrapyFormView(FormView):

    template_name = "admin/products/scrapy.html"
    form_class = ScrapyFormView
    success_url = "/products/"

    @method_decorator(staff_member_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AdminScrapyFormView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        command = form.cleaned_data.get("command")
        scrapy_project = settings.SCRAPY_DIR
        os.chdir(scrapy_project)
        os.system(command)
        return super(AdminScrapyFormView, self).form_valid(form)


class MetricsView(ListView):

    template_name = "products/metrics.html"
    queryset = Product.objects.all()
    context_object_name = "products"
