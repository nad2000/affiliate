#-*- coding: utf-8 -*-
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import ListView
import models

###### Provider
class CreateProvider(CreateView):
  model = models.Provider
  template_name = 'common/create.html'
  fields = ['url', 'name', 'product', 'productPrice', 'moq', 'canShipToChina', 'moqForCustomDesign', 'samplePrice']
  success_url = '/ecmanager/providers/'

  def get_initial(self):
    return { "moq": "1", 'moqForCustomDesign': '1', "samplePrice":"999999" }

class ListProviders(ListView):
  model = models.Provider
  template_name = "ecmanager/list_providers.html"
  context_object_name = 'providers'

  def get_queryset(self):
    return models.Provider.objects.order_by('id')

class UpdateProvider(UpdateView):
  model = models.Provider
  template_name = 'common/update.html'
  success_url = '/ecmanager/providers/'
  fields = CreateProvider.fields + ['serviceQualityScore', 'serviceQualityReview']

  def get_initial(self):
    return { "serviceQualityScore": "5", 'serviceQualityReview': 'dummy' }

class DeleteProvider(DeleteView):
  model = models.Provider
  template_name = 'common/confirm_delete.html'
  success_url = '/ecmanager/ListProvider'

from django.http import HttpResponse
def GetProductPrice(request, providerId): 
  return HttpResponse(Provider.objects.get(id=providerId).productPrice)

###### Order
from models import Provider
from forms import OrderForm
class CreateOrder(CreateView):
  #model = models.Order
  form_class = OrderForm 

  template_name = 'common/create.html'
  success_url = '/ecmanager/orders/'

  # [Default values](http://stackoverflow.com/questions/16927144/createview-validate-form-and-default-fields)
  # does not work. 
  # def get_initial(self):
    # print ("dir(self.request.REQUEST): %s" % dir(self.request.REQUEST))
    # return { 'productPriceForCurrentOrder': 1}

class ListOrders(ListView):
  model = models.Order
  template_name = "ecmanager/list_orders.html"
  context_object_name = 'orders'

  def get_queryset(self):
    return models.Order.objects.order_by('id')

from forms import OrderForm
class UpdateOrder(UpdateView):
  model = models.Order
  template_name = 'common/update.html'
  success_url = '/ecmanager/orders/'
  fields = OrderForm.Meta.fields

class DeleteOrder(DeleteView):
  model = models.Order
  template_name = 'common/confirm_delete.html'
  success_url = '/ecmanager/orders/'

###### Ad
class CreateAd(CreateView):
  model = models.Ad
  fields = ['url', 'product', 'creator', 'comment']

  template_name = 'common/create.html'
  success_url = '/ecmanager/ListAds'

class ListAds(ListView):
  model = models.Ad
  template_name = "ecmanager/list_ads.html"
  context_object_name = 'ads'
  def get_queryset(self):
    return models.Ad.objects.order_by('id')

class UpdateAd(UpdateView):
  model = models.Ad
  template_name = 'common/update.html'
  fields = CreateAd.fields
  success_url = '/ecmanager/ListAds'
