from django.db import models
from django.conf import settings
from apps.products.models import Product
from django.core.validators import MaxValueValidator, MinValueValidator
from decimal import Decimal

class Provider(models.Model):
  url = models.URLField('Provider url', max_length=255)
  name = models.CharField(null=True, default='', max_length=100)
  product = models.ForeignKey(Product, null=True)
  productPrice = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=False)
  moq = models.PositiveIntegerField(null=False)
  samplePrice = models.PositiveIntegerField(null=True)
  # [Enforcing minimum and maximum values in django model fields](http://blog.p3infotech.in/2013/enforcing-minimum-and-maximum-values-in-django-model-fields/)
  serviceQualityScore = models.IntegerField(null=True, default=5, choices=((1,1),(2,2),(3,3),(4,4),(5,5)))
  serviceQualityReview = models.TextField(null=True, default='dummy')
  canShipToChina = models.BooleanField(default=True)
  moqForCustomDesign = models.PositiveIntegerField(null=True)

  def __unicode__(self):
    return "%s, %s, %s, %s" % (self.product.internalId, self.product.title, self.name, self.url)

import datetime
from apps.users.models import User 
OrderStatusChoices = (
  ("ordered", "ordered"),
  ("arrived at China aggregation serivce", "arrived at China aggregation serivce"),
  ("leaved China", "leaved China"),
  ("arrived In Indonesia", "arrived In Indonesia"),
)
Roles = ( 
        ('mei', 'mei'), 
        ('xling', 'xling'), 
        ('carrie', 'carrie'), 
        ('xiaoyu', 'xiaoyu'), 
        ('pin an ji yun', 'pin an ji yun'), 
        )
class Order(models.Model):
  provider = models.ForeignKey(Provider)
  productPriceForCurrentOrder = models.DecimalField(
      max_digits=5, 
      decimal_places=2, 
      blank=True, 
      null=False, 
      validators=[MinValueValidator(Decimal('0.01'))])
  shippingCostInChina = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=False)
  shippingCostToIndonesia = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=False)
  trackingNumberInChina = models.CharField(max_length=255, default='')
  trackingNumberToIndonesia = models.CharField(max_length=255, default='')
  # [cannot use datetime in createview]http://stackoverflow.com/questions/24137457/fielderror-at-admin-unknown-fields-added-on-specified-for-userprofile
  # [editable is needed](http://stackoverflow.com/questions/5499315/form-fields-for-modelform-with-inherited-models)
  orderingDate = models.DateTimeField("OrderingDate: yyyy-mm-dd HH:SS:MM")
  receivingDate = models.DateTimeField("ReceivingDate: yyyy-mm-dd HH:SS:MM", default='2099-12-31', null=True)
  # need to use related_name 
  # [](http://stackoverflow.com/questions/22538563/django-reverse-accessors-for-foreign-keys-clashing)
  # [](https://docs.djangoproject.com/en/1.6/ref/models/fields/#django.db.models.ForeignKey.related_name)
  orderMaker = models.CharField( max_length=100, choices=Roles, default="mei")
  sender = models.CharField( max_length=100, choices=Roles, default="xiaoyu")
  receiver = models.CharField( max_length=100, choices=Roles, default="mei")
  orderQuantity = models.PositiveIntegerField()
  status = models.CharField(max_length=100, choices=OrderStatusChoices, default="ordered")
  comment = models.TextField(max_length=500, default='dummy', null=True)

class Ad(models.Model):
  url = models.URLField('Provider url', max_length=255)
  product = models.ForeignKey(Product)
  creator = models.ForeignKey(User)
  comment = models.TextField(default=" ", null=True)

##### dbg [start] ##### 
class TestModel(models.Model):
  url = models.URLField('Provider url', max_length=255)
  product = models.ForeignKey(Product)
##### dbg [end] ##### 
