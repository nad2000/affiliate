from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from util import RequestStaff
import views

urlpatterns = patterns('',
  # url(r'^CreateProduct$', RequestStaff()(views.CreateProduct.as_view()), name='CreateProduct'),
  # url(r'^ListProducts$', RequestStaff()(views.ListProducts.as_view()), name='ListProducts'),
  url(r'^create-provider/$', RequestStaff()(views.CreateProvider.as_view()), name='create-provider'),
  url(r'^providers/$', RequestStaff()(views.ListProviders.as_view()), name='providers-list'),
  url(r'^update-provider/(?P<pk>.*\d+)/$', RequestStaff()(views.UpdateProvider.as_view()), name='update-provider'),
  url(r'^delete-provider/(?P<pk>.*\d+)/$', RequestStaff()(views.DeleteProvider.as_view()), name='delete-provider'),
  url(r'^GetProductPrice/(?P<providerId>[0-9]+)/$', login_required(views.GetProductPrice), name='GetProductPrice'),

  url(r'^create-order/$', RequestStaff()(views.CreateOrder.as_view()), name='create-order'),
  url(r'^orders/$', RequestStaff()(views.ListOrders.as_view()), name='orders-list'),
  url(r'^update-order/(?P<pk>.*\d+)/$', RequestStaff()(views.UpdateOrder.as_view()), name='update-order'),
  url(r'^delete-order/(?P<pk>.*\d+)/$', RequestStaff()(views.DeleteOrder.as_view()), name='delete-order'),

  url(r'^CreateAd/$', RequestStaff()(views.CreateAd.as_view()), name='CreateAd'),
  url(r'^ListAds/$', RequestStaff()(views.ListAds.as_view()), name='ListAds'),
  url(r'^UpdateAd/(?P<pk>[0-9]+)/$', RequestStaff()(views.UpdateAd.as_view()), name='UpdateAd'),
)

