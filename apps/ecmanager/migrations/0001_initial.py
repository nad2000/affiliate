# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
from decimal import Decimal
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('products', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Ad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField(max_length=255, verbose_name=b'Provider url')),
                ('comment', models.TextField(default=b' ', null=True)),
                ('creator', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('product', models.ForeignKey(to='products.Product')),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('productPriceForCurrentOrder', models.DecimalField(blank=True, max_digits=5, decimal_places=2, validators=[django.core.validators.MinValueValidator(Decimal('0.01'))])),
                ('shippingCostInChina', models.DecimalField(max_digits=5, decimal_places=2, blank=True)),
                ('shippingCostToIndonesia', models.DecimalField(max_digits=5, decimal_places=2, blank=True)),
                ('trackingNumberInChina', models.CharField(default=b'', max_length=255)),
                ('trackingNumberToIndonesia', models.CharField(default=b'', max_length=255)),
                ('orderingDate', models.DateTimeField(verbose_name=b'OrderingDate: yyyy-mm-dd HH:SS:MM')),
                ('receivingDate', models.DateTimeField(default=b'2099-12-31', null=True, verbose_name=b'ReceivingDate: yyyy-mm-dd HH:SS:MM')),
                ('orderMaker', models.CharField(default=b'mei', max_length=100, choices=[(b'mei', b'mei'), (b'xling', b'xling'), (b'carrie', b'carrie'), (b'xiaoyu', b'xiaoyu'), (b'pin an ji yun', b'pin an ji yun')])),
                ('sender', models.CharField(default=b'xiaoyu', max_length=100, choices=[(b'mei', b'mei'), (b'xling', b'xling'), (b'carrie', b'carrie'), (b'xiaoyu', b'xiaoyu'), (b'pin an ji yun', b'pin an ji yun')])),
                ('receiver', models.CharField(default=b'mei', max_length=100, choices=[(b'mei', b'mei'), (b'xling', b'xling'), (b'carrie', b'carrie'), (b'xiaoyu', b'xiaoyu'), (b'pin an ji yun', b'pin an ji yun')])),
                ('orderQuantity', models.PositiveIntegerField()),
                ('status', models.CharField(default=b'ordered', max_length=100, choices=[(b'ordered', b'ordered'), (b'arrived at China aggregation serivce', b'arrived at China aggregation serivce'), (b'leaved China', b'leaved China'), (b'arrived In Indonesia', b'arrived In Indonesia')])),
                ('comment', models.TextField(default=b'dummy', max_length=500, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Provider',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField(max_length=255, verbose_name=b'Provider url')),
                ('name', models.CharField(default=b'', max_length=100, null=True)),
                ('productPrice', models.DecimalField(max_digits=5, decimal_places=2, blank=True)),
                ('moq', models.PositiveIntegerField()),
                ('samplePrice', models.PositiveIntegerField(null=True)),
                ('serviceQualityScore', models.IntegerField(default=5, null=True, choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5)])),
                ('serviceQualityReview', models.TextField(default=b'dummy', null=True)),
                ('canShipToChina', models.BooleanField(default=True)),
                ('moqForCustomDesign', models.PositiveIntegerField(null=True)),
                ('product', models.ForeignKey(to='products.Product', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='TestModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField(max_length=255, verbose_name=b'Provider url')),
                ('product', models.ForeignKey(to='products.Product')),
            ],
        ),
        migrations.AddField(
            model_name='order',
            name='provider',
            field=models.ForeignKey(to='ecmanager.Provider'),
        ),
    ]
