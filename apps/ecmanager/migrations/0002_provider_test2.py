# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecmanager', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='provider',
            name='test2',
            field=models.CharField(default=b'', max_length=100, null=True),
        ),
    ]
