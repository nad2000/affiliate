# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecmanager', '0005_remove_order_sender'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='sender1',
            new_name='sender',
        ),
    ]
