# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecmanager', '0003_auto_20151102_1113'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='test2',
        ),
        migrations.AddField(
            model_name='order',
            name='sender1',
            field=models.CharField(default=b'xiaoyu', max_length=100, choices=[(b'mei', b'mei'), (b'xling', b'xling'), (b'carrie', b'carrie'), (b'xiaoyu', b'xiaoyu'), (b'pin an ji yun', b'pin an ji yun')]),
        ),
    ]
