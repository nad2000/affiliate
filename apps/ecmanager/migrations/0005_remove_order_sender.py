# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecmanager', '0004_auto_20151102_1114'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='sender',
        ),
    ]
