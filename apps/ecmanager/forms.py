# -*- coding: utf-8 -*-
from models import Order, Provider
from django import forms

class OrderForm(forms.ModelForm):
  class Meta:
    model = Order
    fields = [
            'provider', 
            'productPriceForCurrentOrder', 
            'shippingCostInChina', 
            'shippingCostToIndonesia', 
            'trackingNumberInChina', 
            'trackingNumberToIndonesia', 
            'orderingDate', 
            'receivingDate', 
            'orderMaker', 
            'receiver', 
            'orderQuantity', 
            'status', 
            'comment']
		
	# you can overwrite the fields in Meta.fields
  provider = forms.ModelChoiceField(
      queryset = Provider.objects.all(),
      widget=forms.Select(attrs={"onChange":'GetProductPrice()'})
      )
