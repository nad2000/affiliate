from django.conf.urls import patterns, url, include
# from config.views import RegisterTenant
from apps.customers.views import TenantIndexView

urlpatterns = patterns('',
    url(r'^tenants/', include("apps.customers.urls", namespace="tenants")),
    url(r'^$', TenantIndexView.as_view()),
    )
