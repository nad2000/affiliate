#from django.conf.urls import patterns, url
#from apps.customers.views import TenantView
#
#urlpatterns = patterns('',
#                       url(r'^$', TenantView.as_view()),
#                       )

from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.core.urlresolvers import reverse
from django.views.generic.base import RedirectView
admin.autodiscover()

from apps.customers.views import TenantIndexView 

urlpatterns = patterns('',
  
  url(r'^admin/', include(admin.site.urls)),
  url(r'^users/', include("apps.users.urls", namespace="users")),
  url(r'^products/', include("apps.products.urls", namespace="products")),
  url(r'^ecmanager/', include("apps.ecmanager.urls", namespace="ecmanager")),
  url(r'^captcha/', include('captcha.urls')),
  url(r'^$', TenantIndexView.as_view()),
)
